'use strict';

//Extern dependencies
const mysql   = require('promise-mysql');

//Intern configurations
const config  = require('./config');
const pool    = mysql.createPool(config.db_dev);

//Promise to pass queries to MySQL database
const doQuery = (query)=> new Promise((resolve, reject)=> {
    pool.getConnection().
        then((connection)=> {
            connection.query(query).
                then((result)=> {
                    resolve({rows: result});
                }).
                catch((Error)=> {
                    reject(new Error('Error to get the fields from database'));
                }).
                finally(()=> {
                    pool.releaseConnection(connection);
                });
        }).
        catch((Error)=> reject(new Error('Error to connect to database')));
});

module.exports = {
    doQuery
};
