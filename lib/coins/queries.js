'use strict';

//Import internal dependencies
const pool = require('../../config/pool');

const getCoins = (params)=> {
    const query = `SELECT *
             FROM usuario`;

    return pool.doQuery(query,params);
};

module.exports = {
    getCoins
};
