'use strict';

const qry = require('./queries');

const getAllCoins = async (req, res)=> {
    try {
        const result = await qry.getCoins();

        return res.status(200).send(result);
    } catch (Error) {
        return res.status(500).send(new Error('Error to get all users'));
    }
};

module.exports = {
    getAllCoins
};
