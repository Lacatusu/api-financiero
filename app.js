'use strict';

//Extern NPM dependencies
const bodyParser   = require('body-parser');
const express      = require('express');
const cors         = require('cors');

//Init app
const app          = express();
const router       = express.Router();
//Routes
require('./lib/coins')(router);

//Middleware body-parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Use routes
app.use('/',router);

//Cors to allow external connections
app.options('*',cors());

//Port declaration and listening
const port = process.env.PORT || 9090 ;

app.listen(port,()=> console.log(`App listenning on port ${port}`));

